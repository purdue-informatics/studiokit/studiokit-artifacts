﻿namespace StudioKit.Artifacts.Models.Interfaces
{
	public interface IFileArtifact : IArtifact
	{
		string FileName { get; set; }
	}
}