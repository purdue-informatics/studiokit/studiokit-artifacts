﻿using System.IO;

namespace StudioKit.Artifacts.Models.BusinessModels
{
	public interface IArtifactBusinessModel
	{
		int? Id { get; set; }
	}

	public class FileArtifactBusinessModel : IArtifactBusinessModel
	{
		public int? Id { get; set; }
		public string FileName { get; set; }
		public string ContentType { get; set; }
		public Stream FileStream { get; set; }
	}

	public class TextArtifactBusinessModel : IArtifactBusinessModel
	{
		public int? Id { get; set; }
		public string Text { get; set; }
		public int WordCount { get; set; }
	}

	public class UrlArtifactBusinessModel : IArtifactBusinessModel
	{
		public int? Id { get; set; }
		public string Url { get; set; }
	}
}