﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Replay.Utils;
using StudioKit.Artifacts.Models.Interfaces;
using StudioKit.Artifacts.Models.BusinessModels;
using StudioKit.Artifacts.Properties;
using StudioKit.Artifacts.Utils;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Data;
using StudioKit.Scaffolding.Interfaces;

namespace StudioKit.Artifacts
{
	public class ArtifactStorageService
	{
		private const string PlainTextContentType = "text/plain";
		private const string DefaultAllowedFileExtensionsPattern = @"^\.(pdf|txt|doc|docx|xls|xlsx|ppt|pptx|jpg|jpeg|png|gif|bmp|mp4|mov|mp3)$";

		public static async Task<IArtifact> SaveArtifactAsync<TEntity, TArtifact, TTextArtifact, TFileArtifact, TUrlArtifact>(
			IArtifactBusinessModel businessModel, IBlobStorage artifactStorage, string userId, IBaseDbContext dbContext,
			TEntity artifactsContainer)
			where TEntity : ModelBase, IArtifactsContainer<TArtifact>
			where TArtifact : ModelBase, IArtifact
			where TTextArtifact : ITextArtifact, new()
			where TFileArtifact : IFileArtifact, new()
			where TUrlArtifact : IUrlArtifact, new()
		{
			if (artifactsContainer == null) throw new ArgumentNullException(nameof(artifactsContainer));
			if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
			if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
			if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));

			IArtifact artifact = await ModelArtifact<TTextArtifact, TFileArtifact, TUrlArtifact>(businessModel, artifactStorage, userId);

			artifactsContainer.Artifacts.Add((TArtifact)artifact);
			await dbContext.SaveChangesAsync();

			return artifact;
		}

		public static async Task<IArtifact> CreateArtifactAsync<TTextArtifact, TFileArtifact, TUrlArtifact>(
			IArtifactBusinessModel businessModel, IBlobStorage artifactStorage, string userId)
			where TTextArtifact : ITextArtifact, new()
			where TFileArtifact : IFileArtifact, new()
			where TUrlArtifact : IUrlArtifact, new()
		{
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
			if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
			if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));

			var artifact =
				await ModelArtifact<TTextArtifact, TFileArtifact, TUrlArtifact>(businessModel, artifactStorage, userId);

			return artifact;
		}

		public static async Task<IArtifact> UpdateArtifactAsync<TTextArtifact, TFileArtifact, TUrlArtifact>(IArtifact artifact, IArtifactBusinessModel businessModel,
			IBlobStorage artifactStorage, IBaseDbContext dbContext)
			where TTextArtifact : ITextArtifact, new()
			where TFileArtifact : IFileArtifact, new()
			where TUrlArtifact : IUrlArtifact, new()
		{
			if (artifact == null) throw new ArgumentNullException(nameof(artifact));
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
			if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
			if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));

			switch (businessModel)
			{
				case TextArtifactBusinessModel t:
					if (!(artifact is TTextArtifact textArtifact))
					{
						throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
					}

					// To update, delete the existing blob
					var existingTextBlobId = textArtifact.Url.Substring(textArtifact.Url.LastIndexOf("/", StringComparison.Ordinal) + 1);
					var existingTextBlobPath = StorageHelpers.GetExistingTextBlobPath(existingTextBlobId);
					await artifactStorage.DeleteBlockBlobAsync(existingTextBlobPath);

					// And upload the new blob
					var textBlobPath = StorageHelpers.GenerateTextBlobPath();
					var sanitizedText = ArtifactHtmlSanitizeSettings.Sanitize(t.Text);
					using (var textStream = StorageHelpers.GenerateStreamFromString(sanitizedText))
					{
						await artifactStorage.UploadBlockBlobAsync(textStream, textBlobPath, PlainTextContentType);
					}

					textArtifact.WordCount = t.WordCount;
					textArtifact.Url = artifactStorage.UriForBlockBlob(textBlobPath).ToString();

					await dbContext.SaveChangesAsync();
					return textArtifact;

				case FileArtifactBusinessModel f:
					if (!(artifact is TFileArtifact fileArtifact))
					{
						throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
					}

					// To update, delete the existing blob
					var existingFileBlobId = fileArtifact.Url.Substring(fileArtifact.Url.LastIndexOf("/", StringComparison.Ordinal) + 1);
					var existingFileBlobPath = StorageHelpers.GetExistingFileBlobPath(existingFileBlobId); ;
					await artifactStorage.DeleteBlockBlobAsync(existingFileBlobPath);

					// And upload the new blob
					var filename = f.FileName;
					var fileBlobPath = StorageHelpers.GenerateFileBlobPath(filename);
					await artifactStorage.UploadBlockBlobAsync(f.FileStream, fileBlobPath, f.ContentType);

					fileArtifact.FileName = filename;
					fileArtifact.Url = artifactStorage.UriForBlockBlob(fileBlobPath).ToString();

					await dbContext.SaveChangesAsync();
					return fileArtifact;

				case UrlArtifactBusinessModel u:
					if (!(artifact is TUrlArtifact urlArtifact))
					{
						throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
					}

					urlArtifact.Url = u.Url;
					await dbContext.SaveChangesAsync();
					return urlArtifact;

				default:
					throw new InvalidEnumArgumentException();
			}
		}

		public static async Task StreamFileArtifactAsync(IFileArtifact artifact, Stream stream, IBlobStorage artifactStorage)
		{
			if (artifact == null)
				throw new ArgumentNullException(nameof(artifact));
			if (artifact.Url == null)
				throw new ArgumentNullException(nameof(artifact.Url));
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));
			if (artifactStorage == null)
				throw new ArgumentNullException(nameof(artifactStorage));
			var blobPath = StorageHelpers.GetFileBlobPathFromUrl(artifact.Url);
			await artifactStorage.WriteBlockBlobToStreamAsync(stream, blobPath);
		}

		public static async Task<string> CopyBlobAsync(IArtifact source,
			IBlobStorage artifactStorage, CancellationToken cancellationToken = default(CancellationToken))
		{
			var sourcePath = "";
			var destinationPath = "";
			var shouldCopy = false;
			switch (source)
			{
				case ITextArtifact t:
					sourcePath = StorageHelpers.GetTextBlobPathFromUrl(t.Url);
					destinationPath = StorageHelpers.GenerateTextBlobPath();
					shouldCopy = true;
					break;

				case IFileArtifact f:
					sourcePath = StorageHelpers.GetFileBlobPathFromUrl(f.Url);
					destinationPath = StorageHelpers.GenerateFileBlobPath(f.FileName);
					shouldCopy = true;
					break;
			}

			if (!shouldCopy) return null;
			await artifactStorage.CopyBlockBlobAsync(sourcePath, destinationPath, cancellationToken);
			return artifactStorage.UriForBlockBlob(destinationPath).ToString();
		}

		public static void AssertFileExtensionIsValid(IArtifactBusinessModel businessModel, string allowedFileExtensionsPattern = null)
		{
			if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));

			if (!(businessModel is FileArtifactBusinessModel fileArtifactBusinessModel))
			{
				return;
			}

			var currentFileExtension = Path.GetExtension(fileArtifactBusinessModel.FileName)?.ToLowerInvariant();
			if (currentFileExtension == null || !Regex.IsMatch(currentFileExtension, allowedFileExtensionsPattern ?? DefaultAllowedFileExtensionsPattern))
			{
				throw new ValidationException(Strings.FileArtifactExtensionNotAllowed);
			}
		}

		#region private methods

		private static async Task<IArtifact> ModelArtifact<TTextArtifact, TFileArtifact, TUrlArtifact>(
			IArtifactBusinessModel businessModel, IBlobStorage artifactStorage, string userId)
			where TTextArtifact : ITextArtifact, new()
			where TFileArtifact : IFileArtifact, new()
			where TUrlArtifact : IUrlArtifact, new()
		{
			IArtifact artifact;
			switch (businessModel)
			{
				case TextArtifactBusinessModel t:
					var textBlobPath = StorageHelpers.GenerateTextBlobPath();
					var sanitizedText = ArtifactHtmlSanitizeSettings.Sanitize(t.Text);
					using (var textStream = StorageHelpers.GenerateStreamFromString(sanitizedText))
					{
						await artifactStorage.UploadBlockBlobAsync(textStream, textBlobPath, PlainTextContentType);
					}

					artifact = new TTextArtifact
					{
						Url = artifactStorage.UriForBlockBlob(textBlobPath).ToString(),
						CreatedById = userId,
						WordCount = t.WordCount
					};
					break;

				case FileArtifactBusinessModel f:
					var filename = f.FileName;
					var fileBlobPath = StorageHelpers.GenerateFileBlobPath(filename);
					await artifactStorage.UploadBlockBlobAsync(f.FileStream, fileBlobPath, f.ContentType);

					artifact = new TFileArtifact
					{
						FileName = filename,
						Url = artifactStorage.UriForBlockBlob(fileBlobPath).ToString(),
						CreatedById = userId
					};
					break;

				case UrlArtifactBusinessModel u:
					artifact = new TUrlArtifact
					{
						Url = u.Url.Trim(),
						CreatedById = userId
					};
					break;

				default:
					throw new InvalidEnumArgumentException();
			}

			return artifact;
		}

		#endregion private methods
	}
}